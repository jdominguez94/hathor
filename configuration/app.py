from CoupledAdapt import csAgent
import os
import time
import logging
import datetime

from configfiles.functions import Trigger, SyncOnline, VarSp, GetPreferences, Fan


logLevel = os.getenv('LOGLEVEL','INFO')
logging.basicConfig(level=logLevel,format="%(asctime)s [%(name)s] %(message)s",datefmt="[%H:%M:%S]:",)
logger = logging.getLogger(__name__)

if __name__ == "__main__":

    #Log
    ph1=GetPreferences(2,0.6,21,24)
    ph2=GetPreferences(2,0.2,19,20)
    ph3=GetPreferences(2,0,22,22)
    ph4=GetPreferences(2,0.8,21,24)
    ph5=GetPreferences(2,1.5,21,22)
    pref=[ph1,ph2,ph3,ph4,ph5]


    spoints=VarSp()
    trigs=Trigger()
    fan=Fan()


    #
    path = os.path.join(os.getcwd(), 'config.yaml')

    with open(path, 'r') as file:
        Loader = yaml.load(file, Loader=yaml.FullLoader)
        logger.info("Loading config data...")

    # Load configuration
    path = os.path.join(os.getcwd(), 'config_cs.yaml')
    yamlFile = open(path,"r")
    configInfo = yaml.load(yamlFile, Loader=yaml.FullLoader)


    for point in Loader['parameters']:
        logger.info("I found parameters for objects: {}".format(point))
        house_params = Loader['parameters']['House']
        bat_params=Loader['parameters']['ThermalStorage']
        sim_params=Loader['parameters']['simulation']
        influxdata = Loader['parameters']['influxtwo']
        weather = Loader['parameters']['weather']

    name=house_params["fmuname"]
    trigs=Trigger()

    logger.info("Starting agent..." )

    eplus = csAgent(house_params,bat_params,SPf,fan,TRIG,influxdata,sim_params,name)
    
    logger.info("Started Procedure")
    eplus.DoCS_Learning()
    logger.info("Compilation finished. Voici tes resultats")
