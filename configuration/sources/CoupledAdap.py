from xxlimited import Xxo
from pyfmi import load_fmu
import numpy as np
import matplotlib.pyplot as plt
import logging, os, datetime
import pandas as pd
import datetime
import aioschedule as schedule
import asyncio
import nest_asyncio
from Cout import LinearMLPower
import pickle
from mongoAgent import mongoManager
import pandas as pd

nest_asyncio.apply()


from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client .client.write_api import SYNCHRONOUS

#from thermalmodel import HouseModel
from thermalmodelTS import HouseModel


logging.basicConfig(level=logging.INFO,format="%(asctime)s [%(name)s] %(message)s",datefmt="[%H:%M:%S]:",)
logger = logging.getLogger('CoSimulation Agent')

class csAgent():

    """
    Object used for do-step co-simulation through the FMI protocol to model the thermo-energetic environment for a 3 1/2 house

    
    """        

    def __init__(self,house_params,bat_params,sp,fan,trig,influxdata,simparam,name):#):

        #For CoSim
        self.fmudir=house_params['address']
        self.fmudir2=bat_params['address']
        self.start_time=house_params['start']
        self.final_time=house_params['end']
        self.houseid=house_params['fmuname']

        self.step=house_params['step']
        self.sp=sp
        self.fan=fan
        self.stepbattery=bat_params['step']
        self.trig=trig
        self.trainingperiod=simparam['daysoftraining']

        #For Influx
        self.url=influxdata['url']
        self.org=influxdata['org']
        self.token=influxdata['token']
        self.bucket=influxdata['bucket']
        #self.name= influxdata['dataname']
        self.InfluxDBClient = InfluxDBClient(url=influxdata['url'], token=influxdata['token'], org=influxdata['org'])
        self.name=name
        logger.info("The object was created!")
    
    def publish(self,data): 

        self.data= data
        write_client = self.InfluxDBClient.write_api()
        logger.info("Publishing data into Influx...")
        write_client.write(self.bucket, self.org, record=self.data,data_frame_measurement_name=self.name)#self.name)
        logger.info("Done!")

    def GetForecast(self,start,stop,pointer,fields):

        self.start=start 
        self.stop=stop #
        self.fields=fields # As a List of strings
        self.pointer=pointer
        logger.info("Forecast data of: {}, will be requested on period  {} -- {}".format(fields, self.start,self.stop))

        K = 'or '.join(['r["_field"] == "{}" '.format(field) for field in fields])[:-1] 

        query1= '''
                from(bucket: "{}")
                |> range(start: {}, stop: {})
                |> filter(fn: (r) => r["_measurement"] == {})
                |> filter(fn: (r) => {})
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                '''.format(self.bucket,start,stop,self.pointer,K)
        logger.info("K:".format(K))

        api_queryfcast=self.InfluxDBClient.query_api()
        dfcast = api_queryfcast.query_data_frame(query1)
        df=dfcast[self.fields]
        df=df.set_index(dfcast['_time'])

        logger.info("[{}] Query done!".format("Forecast"))
        return df

    def GetSync(self,start,stop,fields):

        self.start=start # Both whis this format -->  "2021-07-02T00:00:00Z"
        self.stop=stop #
        self.fields=fields # As a List of strings
        self.sync={}

        Q = 'or '.join(['r["_field"] == "{}" '.format(field) for field in fields])[:-1] 
        Database='"test"'

        query= '''
                from(bucket: "{}")
                |> range(start: {}, stop: {})
                |> filter(fn: (r) => r["_measurement"] == {})
                |> filter(fn: (r) => {})
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                '''.format(self.bucket,start,stop,Database,Q)
                

        api_query=self.InfluxDBClient.query_api()
        df = api_query.query_data_frame(query)
        
        final=pd.concat([df[self.fields[0]],df[self.fields[1]],df[self.fields[2]],df[self.fields[3]],df[self.fields[4]]],axis=1)
        final=pd.DataFrame(final)
        final=final.iloc[-1]
        
        #ONLY THE LATEST VALUES!!!!!! INITIAL CONDITIONS FOR THE NEXT DAY

        self.sync['pi_heating_demand']=[final['Baseboard1'],final['Baseboard2']]
        self.sync['local_temperature']=[final['TempZone1'],final['TempZone2']]
        self.sync['outdoor_temperature']=final['OutdoorTemp']

        logger.info("[{}] Initial conditions for the next day {}".format("SyncData",self.sync))

        return self.sync

    def SyncOnline(self,df):
        self.syncdata={}
        self.syncdata['pi_heating_demand']=[df['Baseboard1'].values.ravel().item(),df['Baseboard2'].values.ravel().item()]    
        self.syncdata['local_temperature']=[df['TempZone1'].values.ravel().item(),df['TempZone2'].values.ravel().item()]
        self.syncdata['outdoor_temperature']=df['OutdoorTemp'].values.ravel().item()
        
        return self.syncdata

    def GetPreferences(self,order,numberofzones):
        self.order=order
        self.numberofzones=numberofzones
        self.preferences={}

        confort=np.zeros((self.numberofzones,self.order))#For order 1
        confort=confort+1.0#+0.5      
        
        #a1=np.ones(144)*20
        #a1[28:65]=23
        #a1[100:120]=26
    
        #a2=np.ones(144)*22
        
        
        #sp=np.vstack([a1,a2])
        sp=np.array([21,23])
        self.preferences['confort']=confort
        self.preferences['sp']=sp
        self.preferences['maxtemp']=np.array([26,26])
        self.preferences['mintemp']=np.array([18,18])

        logger.info("[{}] {}".format("Preferences",self.preferences))
        return self.preferences

    def DoCS_Learning(self, configInfo):
        import pandas as pd


        newtime=pd.date_range(start='2021-01-01',periods=144*15, freq="10min")
        sp1=np.ones(144*15)*10
        sp1=pd.DataFrame(sp1,index=newtime)
        sp1=np.array(sp1)

        house = load_fmu(self.fmudir,log_level=0)
        house.setup_experiment(stop_time=self.final_time)
        house.initialize(self.start_time, self.final_time,True)
        


        logger.info("Started at: {}".format(str(datetime.datetime.now())))
                
        Ins=[]
        for key, value in house.get_input_list().items():
            Ins.append((key))

        Outs=[]
        for key, value in house.get_output_list().items():
            Outs.append((key))

        logger.info("Found these Input variables for {}: {}".format('House',Ins))
        logger.info("Found these Output variables for {}: {}".format('House',Outs))



        inst=mongoManager(configInfo)
        DC=inst.query()
        house_id = self.houseid if self.houseid in DC.keys() else False
        HouseData=DC[house_id]

        logger.info("[{}] House ID {}".format("agentMongoDB", house_id))

        amountETS=HouseData["amountETS"]
        zonesETS=HouseData['zonesETS']
        amountEB=HouseData["amountEB"]
        zonesEB=HouseData["zonesEB"]
        thermalzones=HouseData["thermalzones"]
        listOfTZ=HouseData["listOfTZ"]


        logger.info("amountETS {} ".format(amountETS))
        logger.info("zonesETS {} ".format(zonesETS))
        logger.info("amountEB {} ".format(amountEB))
        logger.info("zonesEB {} ".format(zonesEB))
        logger.info("thermalzones {} ".format(thermalzones))
        logger.info("listOfTZ {} ".format(listOfTZ))


        varsIN = {}
        varsCS = {}

        if amountETS==0:

            logger.info("Found a house with {} thermal zones".format(thermalzones))
            logger.info("This house has {} EB allocated in zones {}".format(amountEB,zonesEB))
            stepsize=self.step # Global time step
            t_step=0 # Initial time step

            for T in listOfTZ:
                varsCS["Ebpower" + str(T)] = []
                varsIN["Spoint" + str(T)] =   self.sp[0]
                varsCS["SP" + str(T)] = []  
                varsCS["Troom" + str(T)] = []

                logger.info("I will set the following set-points (for zone {}) : {}".format(T, self.sp[T]))

            logger.info("Found a Dict for House containing: {}".format(varsCS.keys()))
            
            i=0
            StepByDay=0
            VarSet=dict(varsCS)  
            VarSet["OutdoorTemp"]=[]

            hasTriggered=False

            while t_step < self.final_time:

                i+=1
                for x in listOfTZ:
                    house.set('Spoint'+str(x), varsIN["Spoint"+str(x)][i])
                    troom=np.float(house.get('Troom'+str(x)))
                    VarSet["Troom"+str(x)].append(troom)
                    
                house.do_step(current_t=t_step, step_size=stepsize, new_step=True)

                #For each thermal zone get:
                for X in zonesEB:
                    VarSet["Ebpower"+str(X)].append(np.float(house.get('BaseboardZ'+str(X))))
                    VarSet["SP"+str(X)].append(np.float(house.get('SP'+str(X))))
     
                tout=np.float(house.get("OutdoorTemp"))
                VarSet["OutdoorTemp"].append(tout)

                ##Region to count the days of simulations
                if t_step%86400==0: 
                    StepByDay+=1
                    logger.info("I'm on day {} of simulation".format(StepByDay-1))

                t_step += stepsize

        

        else:

            Batt = {}        
            varsBatt = {}
            varsIN = {}
            varsCS = {}

            logger.info("Found a house with {} thermal zones".format(thermalzones))
            logger.info("This house has {} ETS allocated in zones {}".format(amountETS,zonesETS))
            logger.info("This house has {} EB allocated in zones {}".format(amountEB,zonesEB))

            for U in zonesETS:
                varsBatt["Tcr" + str(U)] = []
                varsBatt["Q_TES" + str(U)] = [] 
                #Creating the set of variables for CS
                Batt["ETS" + str(U)] = load_fmu(self.fmudir2,log_level=0)
                varsIN["Control" + str(U)] = self.fan #Input
                varsIN["Trigger" + str(U)] = self.trig #Input
                varsBatt["POWER_TES" + str(U)] = [] #Output
                varsBatt["POWER_FAN_TES" + str(U)] = [] #Output


            for j in range(amountETS):

                pointer_batt = [s for s in Batt if "ETS" in s]
                #Set up and Initialization of Batteries
                Batt[pointer_batt[j]].setup_experiment(stop_time=self.final_time)
                Batt[pointer_batt[j]].initialize(self.start_time, self.final_time,True)

            logger.info("Found a Dict for ETS containing: {}".format(varsBatt.keys()))


            stepsize=self.step # Global time step
            stepsizeBat=self.stepbattery # ETS time step
        
            t_step=0 # Initial time step
            t_step2=0              # Initial ETS step


            for H in listOfTZ:
                varsCS["Troom" + str(H)] = []

            kk=0
            for T in listOfTZ:
                kk=+1
                varsCS["Ebpower" + str(T)] = []
                varsIN["Spoint" + str(T)] =   self.sp[T]
                varsCS["SP" + str(T)] = []  
                logger.info("I will set the following set-points (for zone {}) : {}".format(T, self.sp[T]))

            logger.info("Found a Dict for House containing: {}".format(varsCS.keys()))
            
            VarSet=dict(varsBatt, **varsCS)  
            VarSet["OutdoorTemp"]=[]

            i=0
            StepByDay=0

            hasTriggered=False

            while t_step < self.final_time:
                i+=1
                ac=0
                ca=0
                    

                for x in listOfTZ:
                    if x in zonesETS:
                        house.set('Spoint'+str(x), sp1[i])  
                        troom=np.float(house.get('Troom'+str(x)))
                        VarSet["Troom"+str(x)].append(troom)  
                    else:   
                        #Setting set point to 10 to avoid turning on the EB
                        house.set('Spoint'+str(x), varsIN["Spoint"+str(x)][i])
                        troom=np.float(house.get('Troom'+str(x)))
                        VarSet["Troom"+str(x)].append(troom)
                
                
                
                house.do_step(current_t=t_step, step_size=600, new_step=True)

                #Setting the Indoor temperature of each TZ to each battery 
                for ac in range(amountETS):
                    for ca in range(len(zonesETS)):
                        Batt[pointer_batt[ac]].set('Troom', house.get('Troom'+str(zonesETS[ca])))

                ##Region to Run the ETS

                latestQval={}
                ## Setting power values for ETS
                while t_step2<= t_step:#stepsize

                    for y in zonesETS:
                        Batt["ETS"+str(y)].set('Power',      varsIN["Trigger"+str(y)][i])
                        Batt["ETS"+str(y)].set('Control',    varsIN["Control"+str(y)][i])
                        Batt["ETS"+str(y)].do_step(current_t=t_step2, step_size=stepsizeBat, new_step=True)
                        latestQval["LatQ"+str(y)]=Batt["ETS"+str(y)].get('OutputF') #MAL

                    t_step2 += stepsizeBat

                for x in zonesETS:
                    house.set('Q'+str(x), latestQval["LatQ"+str(x)])

                ####################    DATA FOR INFLUXDB   ########################

                #For each battery request:
                for k in zonesETS:
                    VarSet["POWER_TES"+str(k)].append(np.float(Batt["ETS"+str(k)].get('Power')))
                    VarSet["POWER_FAN_TES"+str(k)].append(np.float(Batt["ETS"+str(k)].get('Control')))
                    VarSet["Tcr"+str(k)].append(np.float(Batt["ETS"+str(k)].get('Tcr')))
                    VarSet["Q_TES"+str(k)].append(np.float(Batt["ETS"+str(k)].get('OutputF')))

                #For each thermal zone get:
                for X in zonesEB:

                    VarSet["Ebpower"+str(X)].append(np.float(house.get('BaseboardZ'+str(X))))
                    VarSet["SP"+str(X)].append(np.float(house.get('SP'+str(X))))

                tout=np.float(house.get("OutdoorTemp"))

                VarSet["OutdoorTemp"].append(tout)

                ##Region to count the days of simulations
                if t_step%86400==0: 
                    StepByDay+=1
                    logger.info("I'm on day {} of simulation".format(StepByDay-1))

                t_step += stepsize

                import pandas as pd
        
        
        import pandas as pd

        timereal=pd.date_range(start='2021-01-01',periods=144*self.trainingperiod, freq="10min")

        for x in VarSet.keys():
            print(x,len(VarSet[x]))

        df=pd.DataFrame.from_dict(VarSet)
        df.index=timereal
        
        self.publish(df)

        return df, varsIN, VarSet
       
